import java.util.Scanner;

class Manusia {
    String nama;
    KreditSosial kredit;
    Manusia(String nama, int nilai) {
        this.nama = nama;
        this.kredit = new KreditSosial(nilai);
    }
    
    void AksiBaik() {
        System.out.println(this.nama+" melakukan kebaikan, kredit ditambah 100.");
        this.kredit.penambahanNilai();
    }
    
    void AksiBuruk() {
        System.out.println(this.nama+" melakukan keburukan, kredit dikurangi 150.");
        this.kredit.penguranganNilai();
        if (this.kredit.getNilai() < -100) {
            System.out.println(this.nama+" ditangkap oleh pihak kepolisian.");
        }
    }
}
class KreditSosial {
    private int nilai;
    KreditSosial(int nilai) {
        this.nilai = nilai;
    }
    
    void penambahanNilai() {
        this.nilai+= 100;
    }
    
    void penguranganNilai() {
        this.nilai-= 150;
    }
    int getNilai() {
        return this.nilai;
    }
}
class FasilitasUmum {
    void fasilitasUmum(Manusia man) {
        if (man.kredit.getNilai() > 0) {
            System.out.println(man.nama+" dapat menikmati banyak Fasilitas Umum.");
        } else {
            System.out.println(man.nama+" tidak dapat menikmati Fasilitas Umum.");
        }
    }
}
public class SimulasiSistemKreditSosial {
    public static void main(String[] args) {
    	Scanner read = new Scanner(System.in);
    	Manusia[] man = new Manusia[100];
        FasilitasUmum fas = new FasilitasUmum();
        int k = 0;
        view();

        while (true) {
    		System.out.print("\nPerintah : ");
    		String[] cmd = read.nextLine().split(" ", 3);
        	System.out.println("----------------------------------------");

	    	if (cmd[0].equals("buat")) {
    			man[k++] = new Manusia(cmd[1], Integer.parseInt(cmd[2]));
    			System.out.println("Berhasil membuat objek manusia [m"+k+"]");
    		} else if (cmd[0].equals("stop")) {
    			System.out.println("Program dihentikan");
    			System.exit(0);
    		} else if (cmd[0].equals("manusia")) {
    			System.out.println("\nList Objek Manusia");
    			System.out.println("m[no] \t[nama]");
    			int a = 0;
    			while (a < k) {
    				System.out.println("m"+(a+1)+" \t"+man[a++].nama);
    			}
    		} else {
    			int nomor = Character.getNumericValue(cmd[0].charAt(1))-1;
	    		System.out.println("Hasil : ");
    			if (cmd[1].equals("aksibaik")) {
 	   				man[nomor].AksiBaik();
		    	} else if (cmd[1].equals("aksiburuk")) {
    				man[nomor].AksiBuruk();
    			} else if (cmd[1].equals("cekfasil")) {
    				fas.fasilitasUmum(man[nomor]);
    			}
    		}
        }
    }
    static void view() {
        System.out.println("========================================");
        System.out.println("|    Simulasi Perilaku Warga Cibiru    |");
        System.out.println("========================================");
        System.out.println("Contoh Perintah");
        System.out.println("{stop}, untuk menghentikan program");
        System.out.println("{buat <nama> <nilai>}, membuat objek");
        System.out.println("{manusia}, list objek manusia");
        System.out.println("{m<no> <perilaku>}, beraksi");
        System.out.println("[no], nomor objek");
        System.out.println("[perilaku], aksibaik aksiburuk cekfasil");
        System.out.println("----------------------------------------");
    }
}
